import React, { useRef, useEffect, useState } from 'react';
import { Form } from '@unform/web';
import { Scope } from '@unform/core';
import * as Yup from 'yup';
import './App.css';
import Input from './components/Form/Input';

const initialData = {
  email: 'ernanijose@gmail.com',
  address: {
    street: "Rua Aqui",
    city: 'Santa Maria'
  }
}

function App() {  

  /*
  const user = {
    name: "Ernani",
    address: {
      street: "Rua Aqui",
      number: 123
    }
  }
  */
  //const [user, setUser] = useState({});
  const formRef = useRef(null);

  async function handleSubmit(data, { reset }){
    //console.log(formRef.current);
    /*
    formRef.current.setFieldError('name', 'O nome é obrigatório');
    formRef.current.setFieldError('address.state', 'O estado é obrigatório');
    */
    /*
    formRef.current.setErrors({
      name: 'O nome é obrigatório',
      address: {
        state: 'O estado é obrigatório'
      }
    });
    */

    try{
      const schema = Yup.object().shape({
        name: Yup.string().required('O nome é obrigatório!'),
        email: Yup.string().email('Digite um e-mail válido!').required('O e-mail é obrigatório!'),
        address: Yup.object().shape({
          city: Yup.string().min(3, 'No minimo 3 caracteres!').required('O campo cidade é obrigatório!')
        })
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      console.log(data);
      
      reset();
    } catch(err) {
      if(err instanceof Yup.ValidationError) {
        const errorMessages = {};

        err.inner.forEach(error => {
          errorMessages[error.path] = error.message;
        });

        formRef.current.setErrors(errorMessages);
      }
    }
  }

  useEffect(() => {
    setTimeout(() => {
      formRef.current.setData({
        name: 'Ernani Jose',
        email: 'ernanijose@gmail.com',
        address: {
          city: 'Santa Maria'
        }
      });
    }, 2000);
  }, []);

  return (
    <div className="App">
      <h1>Hello Word!</h1>

      <Form ref={formRef} onSubmit={handleSubmit}>
        <Input name="name" />
        <Input type="email" name="email" />
        <Input type="password" name="password" />

        <Scope path="address">
          <Input name="street" />
          <Input name="city" />
          <Input name="state" />
          <Input name="number" />
        </Scope>
        <button type="submit">Enviar</button>
      </Form>
    </div>
  );
}

export default App;
